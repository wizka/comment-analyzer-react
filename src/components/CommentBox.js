import Card from 'react-bootstrap/Card';

function TextExample(text) {
  return (
    <Card>
      <Card.Body>
        <Card.Text>
          {text}
        </Card.Text>
        <Card.Link href="javascript:;">Analyze</Card.Link>
      </Card.Body>
    </Card>
  );
}

export default TextExample;