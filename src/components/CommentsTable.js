import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

function CommentsTable(comments) { 
  return(
    <div className="row justify-content-md-center">
      {comments.comments.map((comment, index) => {
          return (
            <div key={index} style={{ marginTop: '5px' }} className="col-4">
              <Card>
                <Card.Body>
                  <Card.Text>
                    {comment.text}
                  </Card.Text>
                </Card.Body>
              </Card>
            </div>
          );
      })}
    </div>
  );
}

export default CommentsTable;