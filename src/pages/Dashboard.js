import React,{ useEffect, useState } from 'react'
import axios from 'axios'
import { useNavigate } from "react-router-dom"
import Layout from "../components/Layout"
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import Modal from 'react-bootstrap/Modal';
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid } from 'recharts';
import ListGroup from 'react-bootstrap/ListGroup';
import Badge from 'react-bootstrap/Badge';


const getPath = (x, y, width, height) => {
    return `M${x},${y + height}C${x + width / 3},${y + height} ${x + width / 2},${y + height / 3}
    ${x + width / 2}, ${y}
    C${x + width / 2},${y + height / 3} ${x + (2 * width) / 3},${y + height} ${x + width}, ${y + height}
    Z`;
};

const TriangleBar = (props) => {
    const { fill, x, y, width, height } = props;
  
    return <path d={getPath(x, y, width, height)} stroke="none" fill={fill} />;
};
  
function Dashboard() {
    const navigate = useNavigate();
    const INITIAL_LABEL_STATE = 'Loading...'
    const [comments, setComments] = useState([])
    const [show, setShow] = useState(false);
    const [chart, setChart] = useState({})
    const [validationErrors, setValidationErrors] = useState("");

    const [text, setText] = useState(INITIAL_LABEL_STATE)
    const [likes, setLikes] = useState(INITIAL_LABEL_STATE)
    const [shares, setShares] = useState(INITIAL_LABEL_STATE)
    const [comments_of_comment, setCommentsOfComment] = useState(INITIAL_LABEL_STATE)
    const [neutral, setNeutral] = useState(INITIAL_LABEL_STATE)
    const [negative, setNegative] = useState(INITIAL_LABEL_STATE)
    const [positive, setPositive] = useState(INITIAL_LABEL_STATE)

    const colors = ['#0088FE', '#00C49F', '#FFBB28'];


  const handleClose = () => {
        setShow(false)
        setText(INITIAL_LABEL_STATE)
        setLikes(INITIAL_LABEL_STATE)
        setShares(INITIAL_LABEL_STATE)
        setCommentsOfComment(INITIAL_LABEL_STATE)
        setNeutral(INITIAL_LABEL_STATE)
        setNegative(INITIAL_LABEL_STATE)
        setPositive(INITIAL_LABEL_STATE)
    };
  const handleShow = () => setShow(true);
 
    useEffect(()=>{
        if(localStorage.getItem('token') == "" || localStorage.getItem('token') == null){
            navigate("/");
        }else {
            getComments();
        }
    },[])

    const getComments = () => {
        axios.get('/comment/list', { headers:{Authorization: 'Token ' + localStorage.getItem('token')}})
        .then((r) => {
            if(r.data){
                setValidationErrors("")
                setComments(r.data.data);
            }
        })
        .catch((e) => {
            console.log("", e)
            if (e.code){
                if(e.code == "ERR_NETWORK") {
                    setValidationErrors("Cannot connect with server. Please check out your configuration");
                }
                if (e.response) {
                    if (e.response.status != 200) {
                        setValidationErrors(e.response.statusText);
                    }
                }
            } else {
                if (e.response.status != 200) {
                    setValidationErrors(e.response.statusText);
                }
            }
        });
    };
 
    const logoutAction = () => {
        axios.post('/authentication/logout',{}, { headers:{Authorization: 'Token ' + localStorage.getItem('token')}})
        .then((r) => {
            setValidationErrors("")
            localStorage.setItem('token', "")
           navigate("/");
        })
        .catch((e) => {
            if (e.code){
                if(e.code == "ERR_NETWORK") {
                    setValidationErrors("Cannot connect with server. Please check out your configuration");
                }

                if (e.response) {
                    if (e.response.status != 200) {
                        setValidationErrors(e.response.statusText);
                    }
                }
            } else {
                if (e.response.status != 200) {
                    setValidationErrors(e.response.statusText);
                }
            }
        });
    }

    const analyze = (text_input, likes, comments, shares) => {
        handleShow();
        axios.get('/emotion/comment-analysis?input_text=' + text_input , { headers:{Authorization: 'Token ' + localStorage.getItem('token')}})
        .then((r) => {
          setText(text_input)
          setLikes(likes)
          setShares(shares)
          setCommentsOfComment(comments)
          setNeutral(r.data[0][0].score)
          setNegative(r.data[0][1].score)
          setPositive(r.data[0][2].score)
          setChart([
            {
                label: 'Neutral',
                score: r.data[0][0].score
            },
            {
                label: 'Negative',
                score: r.data[0][1].score,
            },
            {
                label: 'Positive',
                score: r.data[0][2].score,
                
            }
          ])
          setValidationErrors("")
        })
        .catch((e) => {
            console.log("error in dashboard analisys", e)
            if (e.code){
                if(e.code == "ERR_NETWORK") {
                    setValidationErrors("Cannot connect with server. Please check out your configuration");
                }
                if (e.response) {
                    if (e.response.status != 200) {
                        setValidationErrors(e.response.statusText);
                    }
                }
            } else {
                if (e.response.status != 200) {
                    setValidationErrors(e.response.statusText);
                }
            }
        });
    };
     
    return (
        <Layout>
            <Modal show={show} size="lg" onHide={handleClose}>
                <Modal.Header closeButton>
                <Modal.Title>Comment Analysis</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {validationErrors != "" &&
                        <div style={{marginTop:'20px'}} className="row justify-content-md-center">
                            <div class="col-9">
                                <Alert variant='danger'>
                                    {validationErrors}, close and try again.
                                </Alert>
                            </div>
                        </div>
                    }
                    {positive === INITIAL_LABEL_STATE && validationErrors == "" ?
                        <Alert variant='info'>
                            Please wait few seconds... we are loading comment analysis.
                        </Alert>
                    :
                        <div>

                            {positive != INITIAL_LABEL_STATE &&
                                <div style={{ marginTop: '20px' }}>
                                    <h3>{text}</h3>
                                    <span style={{ marginTop: '15px' }}></span>
                                    <BarChart
                                        width={500}
                                        height={300}
                                        data={chart}
                                    >
                                        <CartesianGrid strokeDasharray="3 3" />
                                        <XAxis dataKey="label" />
                                        <YAxis />
                                        <Bar dataKey="score" fill="#8884d8" shape={<TriangleBar />} label={{ position: 'top' }}>
                                            {chart.map((entry, index) => (
                                                <Cell key={`cell-${index}`} fill={colors[index % 20]} />
                                            ))}
                                        </Bar>
                                    </BarChart>
                                    <ListGroup>
                                        <ListGroup.Item>
                                            <h6>
                                                {likes} <Badge bg="danger">Likes</Badge>
                                            </h6>
                                        </ListGroup.Item>
                                        <ListGroup.Item>
                                            <h6>
                                                {comments_of_comment} <Badge bg="secondary">Comments</Badge>
                                            </h6>
                                        </ListGroup.Item>
                                        <ListGroup.Item>
                                            <h6>
                                                {shares} <Badge bg="success">Shares</Badge>
                                            </h6>
                                        </ListGroup.Item>
                                    </ListGroup>
                                </div>
                        }
                    </div>
                    }
                </Modal.Body>
                <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                    Close
                </Button>
                </Modal.Footer>
            </Modal>
           <div className="row justify-content-md-center">
                <div className="col-12">
                    <nav className="navbar navbar-expand-lg navbar-light bg-light">
                        <div className="container-fluid">
                            <a className="navbar-brand" href="#">Comments Analyzer</a>
                            <div className="d-flex">
                                <ul className="navbar-nav">
                                    <li className="nav-item">
                                        <a onClick={()=>logoutAction()} className="nav-link " aria-current="page" href="#">Logout</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            {comments.length > 0 && validationErrors == ""  ?
                <div className="row justify-content-md-center">
                    {comments.map((comment, index) => {
                        return (
                            <div key={index} style={{ marginTop: '5px' }} className="col-4">
                                <Card>
                                    <Card.Body>
                                    <Card.Text>
                                        {comment.text}
                                    </Card.Text>
                                    <Button onClick={() => { analyze(comment.text, comment.likes, comment.comments, comment.shares) }} variant="primary">Analyze</Button>
                                    </Card.Body>
                                </Card>
                            </div>
                        );
                    })}
                </div>
                :
                <div style={{marginTop:'20px'}} className="row justify-content-md-center">
                    <div class="col-9">
                        <Alert variant='info'>
                            Please wait few seconds... we are loading comments.
                        </Alert>
                    </div>
                </div>
            }
            {validationErrors != "" &&
                <div style={{marginTop:'20px'}} className="row justify-content-md-center">
                    <div class="col-9">
                        <Alert variant='danger'>
                            {validationErrors}, close and try again.
                        </Alert>
                    </div>
                </div>
            }
        </Layout>
    );
}
   
export default Dashboard;