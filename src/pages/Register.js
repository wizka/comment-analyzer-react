import React,{ useState, useEffect } from 'react'
import axios from 'axios'
import { Link, useNavigate } from "react-router-dom"
import Layout from "../components/Layout"
  
function Register() {
    const navigate = useNavigate();
    const [username, setUsername] = useState("")
    const [name, setName] = useState("")
    const [lastname, setLastname] = useState("")
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [confirmPassword, setConfirmPassword] = useState("")
    const [validationErrors, setValidationErrors] = useState("");
    const [isSubmitting, setIsSubmitting] = useState(false);
 
    useEffect(()=>{
        if(localStorage.getItem('token') != "" && localStorage.getItem('token') != null){
            navigate("/dashboard");
        }
    },[])
 
    const registerAction = (e) => {
        e.preventDefault();
        setIsSubmitting(true)
        let payload = {
            username: username,
            first_name: name,
            last_name: lastname,
            email:email,
            password1:password,
            password2:confirmPassword
        }
        axios.post('/authentication/register', payload)
        .then((r) => {
            console.log(r);
            setIsSubmitting(false)
            alert("Welcome " + username + ", please use your username and password, and go to dashboard.")
            navigate("/");
        })
        .catch((e) => {
            setIsSubmitting(false)
            if (e.code){
                if(e.code == "ERR_NETWORK") {
                    setValidationErrors("Cannot connect with server. Please check out your configuration");
                }

                if (e.response) {
                    if (e.response.status != 200) {
                        setValidationErrors(e.response.statusText);
                    }
                }
            } else {
                if (e.response.status != 200) {
                    setValidationErrors(e.response.statusText);
                }
            }
        });
    }
     
    return (
        <Layout>
            <div className="row justify-content-md-center mt-5">
                <div className="col-4">
                    <div className="card">
                        <div className="card-body">
                            <h5 className="card-title mb-4">Register</h5>
                            {validationErrors != "" &&
                                <p className='text-center '><small className='text-danger'>{validationErrors}</small></p>
                            }
                            <form onSubmit={(e)=>registerAction(e)}>
                            <div className="mb-3">
                                    <label 
                                        htmlFor="username"
                                        className="form-label">Username
                                    </label>
                                    <input 
                                        type="text"
                                        className="form-control"
                                        id="username"
                                        name="username"
                                        value={username}
                                        onChange={(e)=>{setUsername(e.target.value)}}
                                    />
                                     
                                </div>

                                <div className="mb-3">
                                    <label 
                                        htmlFor="name"
                                        className="form-label">Name
                                    </label>
                                    <input 
                                        type="text"
                                        className="form-control"
                                        id="name"
                                        name="name"
                                        value={name}
                                        onChange={(e)=>{setName(e.target.value)}}
                                    />
                                     
                                </div>

                                <div className="mb-3">
                                    <label 
                                        htmlFor="lastname"
                                        className="form-label">Lastname
                                    </label>
                                    <input 
                                        type="text"
                                        className="form-control"
                                        id="lastname"
                                        name="lastname"
                                        value={lastname}
                                        onChange={(e)=>{setLastname(e.target.value)}}
                                    />
                                     
                                </div>

                                <div className="mb-3">
                                    <label 
                                        htmlFor="email"
                                        className="form-label">Email address
                                    </label>
                                    <input 
                                        type="email"
                                        className="form-control"
                                        id="email"
                                        name="email"
                                        value={email}
                                        onChange={(e)=>{setEmail(e.target.value)}}
                                    />
                                     
                                </div>
                                <div className="mb-3">
                                    <label 
                                        htmlFor="password"
                                        className="form-label">Password
                                    </label>
                                    <input 
                                        type="password"
                                        className="form-control"
                                        id="password"
                                        name="password"
                                        value={password}
                                        onChange={(e)=>setPassword(e.target.value)}
                                    />
                                </div>
                                <div className="mb-3">
                                    <label 
                                        htmlFor="confirm_password"
                                        className="form-label">Confirm Password
                                    </label>
                                    <input 
                                        type="password"
                                        className="form-control"
                                        id="confirm_password"
                                        name="confirm_password"
                                        value={confirmPassword}
                                        onChange={(e)=>setConfirmPassword(e.target.value)}
                                    />
                                </div>
                                <div className="d-grid gap-2">
                                    <button 
                                        disabled={isSubmitting}
                                        type="submit"
                                        className="btn btn-primary btn-block">Register Now
                                    </button>
                                    <p 
                                        className="text-center">Have already an account <Link to="/">Login here</Link>
                                    </p>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    );
}
   
export default Register;